import 'package:flutter/material.dart';
import 'package:project_gyb_estimator/employee.dart';
import '../screens/entityCard.dart';

class SeeEntity extends StatelessWidget {
  SeeEntity(this.name, this.firstname, this.mensualIncome, this.mensualCost);

  final String name;
  final String firstname;
  final String mensualIncome;
  final String mensualCost;

  getRenta(mensualCost, mensualIncome) {
    num income = int.tryParse(mensualIncome);
    num cost = int.tryParse(mensualCost);
    num result = ((income / cost) * 100).round();
    num resultToShow = 100 - result;
    return Text(resultToShow.toString() + " %");
  }

    getRfr(mensualCost, mensualIncome) {
    num income = int.tryParse(mensualIncome);
    num cost = 5555;
    num result = ((income / cost) * 100).round();
    num resultToShow = 100 - result;
    print(result);
    return Text(resultToShow.toString() + " %");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(name + " " + firstname),
          elevation: 0,
        ),
        body: Center(
            child: Card(
                clipBehavior: Clip.antiAlias,
                child: Column(
                  children: [
                    ListTile(
                      leading: Icon(Icons.call_received),
                      title: Text(mensualCost),
                      subtitle: Text(
                        'Cout du collaborateur',
                        style: TextStyle(color: Colors.black.withOpacity(0.6)),
                      ),
                    ),
                    ListTile(
                      leading: Icon(Icons.euro),
                      title: Text(mensualIncome),
                      subtitle: Text(
                        'Revenus du collaborateur',
                        style: TextStyle(color: Colors.black.withOpacity(0.6)),
                      ),
                    ),
                    ListTile(
                      leading: Icon(Icons.call_missed_outgoing),
                      title: getRenta(mensualCost, mensualIncome),
                      subtitle: Text(
                        'PPR (Per project rentability)',
                        style: TextStyle(color: Colors.black.withOpacity(0.6)),
                      ),
                    ),
                    ListTile(
                      leading: Icon(Icons.call_missed_outgoing),
                      title: getRfr(mensualCost, mensualIncome),
                      subtitle: Text(
                        'RFR (Rentability for company)',
                        style: TextStyle(color: Colors.black.withOpacity(0.6)),
                      ),
                    ),
                  ],
                ))));
  }
}
