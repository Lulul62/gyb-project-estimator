import '../screens/editEntity.dart';
import 'package:flutter/material.dart';

class EditEntityRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Modifier le collaborateur"),
        elevation: 0,
      ),
      body: Center(
        child: EditEntity(),
      ),
    );
  }
}