class EmployeeList {
  final String name;
  final String firstname;
  final String mensualIncome;
  final String mensualCost;
  final String contractType;

  EmployeeList(this.name, this.firstname, this.mensualIncome, this.mensualCost, this.contractType);
}