import 'package:flutter/material.dart';
import 'package:project_gyb_estimator/employee.dart';

void main() => runApp(SeeEntity());

// Create a Form widget.
class SeeEntity extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<SeeEntity> {
  Employee newEmployee = new Employee();
  final _formKey = GlobalKey<FormState>();

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        margin: const EdgeInsets.only(
            left: 40.0, right: 40.0, bottom: 50.0, top: 50.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: TextFormField(
                  decoration: InputDecoration(
                    hintText: 'Nom',
                    contentPadding: new EdgeInsets.symmetric(
                        vertical: 25.0, horizontal: 10.0),
                  ),
                  onSaved: (val) => newEmployee.name = val,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Veuillez renseigner un nom';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: TextFormField(
                  decoration: InputDecoration(
                    hintText: 'Prénom',
                    contentPadding: new EdgeInsets.symmetric(
                        vertical: 25.0, horizontal: 10.0),
                  ),
                  onSaved: (val) => newEmployee.firstname = val,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Veuillez renseigner un prénom';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: TextFormField(
                  decoration: InputDecoration(
                    hintText: 'Côut mensuel',
                    contentPadding: new EdgeInsets.symmetric(
                        vertical: 25.0, horizontal: 10.0),
                  ),
                  onSaved: (val) => newEmployee.mensualCost = val,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Veuillez renseigner le côut du collaborateur';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: 'Income',
                      contentPadding: new EdgeInsets.symmetric(
                          vertical: 25.0, horizontal: 10.0),
                    ),
                    onSaved: (val) => newEmployee.mensualIncome = val,
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Veuillez renseigner l'income du collaborateur";
                      }
                      return null;
                    },
                  )),
              Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 32.0),
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.orange)),
                    color: Colors.orange,
                    textColor: Colors.white,
                    elevation: 0,
                    onPressed: () {
                      // Validate returns true if the form is valid, or false
                      // otherwise.R
                    },
                    child: Text("Retour à l'accueil"),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
