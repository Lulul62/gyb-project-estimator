import 'package:flutter/material.dart';
import './routes/SeeEntity.dart';
import 'package:flutter/services.dart';
import 'employeeService.dart';
import './routes/editEntity.dart';
import './screens/addEntity.dart';

void main() {
  runApp(MaterialApp(
    title: 'Navigation Basics',
    home: FirstRoute(),
    debugShowCheckedModeBanner: false,
    theme: ThemeData(primaryColor: Colors.white),
  ));
}

void _settingModalBottomSheet(context, currentEmployee) {
  final Object employee = currentEmployee;
  showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return Container(
          child: new Wrap(
            children: <Widget>[
              new ListTile(
                  leading: new Icon(Icons.delete),
                  title: new Text('Supprimer'),
                  onTap: () => {_showMyDialog(context, currentEmployee)}),
              new ListTile(
                leading: new Icon(Icons.edit),
                title: new Text('Modifier'),
                onTap: () => {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => EditEntityRoute()),
                  )
                },
              ),
              new ListTile(
                leading: new Icon(Icons.remove_red_eye),
                title: new Text('Voir'),
                onTap: () => {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SeeEntity(currentEmployee['name'], currentEmployee['firstname'], currentEmployee['mensualCost'], currentEmployee['mensualIncome'])),
                  )
                },
              ),
            ],
          ),
        );
      });
}

Future<void> _showMyDialog(context, currentEmployee) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Supprimer le collaborateur'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(
                  'Attention, cette action est définitive. Souhaitez vous vraiment supprimer ce collaborateur ?'),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Supprimer'),
            onPressed: () async {
            
              await contactService.deleteEmployee(currentEmployee['key']);
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => FirstRoute()),
                );
            },
          ),
          FlatButton(
            child: Text('Annuler'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

var contactService = new ContactService();

Future getEmployees() async {
  var employeesFinal = [];
  var employees = await contactService.getEmployees();
  for (var employee in employees.keys) {
    employees[employee]['key'] = employee;
    employeesFinal.add(employees[employee]);
  }
  return employeesFinal;
}

Widget EmployeesList() {
  return FutureBuilder(
      future: getEmployees(),
      builder: (BuildContext context, AsyncSnapshot projectSnap) {
        if (projectSnap.data == null) {
          return Container(child: Center(child: Text("Loading...")));
        }
        return ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemCount: projectSnap.data.length,
          itemBuilder: (BuildContext context, int index) {
            return Card(
                child: ListTile(
              leading: Icon(Icons.insert_emoticon),
              title: Text(projectSnap.data[index]['name']),
              subtitle: Text(projectSnap.data[index]['firstname']),
              trailing: FlatButton(
                  child: Icon(Icons.more_vert),
                  onPressed: () => _settingModalBottomSheet(
                      context, projectSnap.data[index]),
                  color: Colors.transparent),
            ));
          },
        );
      });
}

class FirstRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.white, //or set color with: Color(0xFF0000FF)
    ));

    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          centerTitle: true,
          title: Text('Liste des collaborateurs'),
          elevation: 0,
        ),
        body: Column(children: [
          Center(
            child: EmployeesList(),
          ),
        ]),
        floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SecondRoute()),
                );
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.red,
      ));
  }
}

class SalesData {
  SalesData(this.year, this.sales);
  final String year;
  final double sales;
}

class SecondRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ajouter un collaborateur"),
        elevation: 0,
      ),
      body: Center(
        child: MyCustomForm(),
      ),
    );
  }
}
