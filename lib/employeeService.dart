import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:project_gyb_estimator/employee.dart';

import 'employee.dart';

class ContactService {
  static const _serviceUrl =
      'https://gyb-estimator.firebaseio.com/employees.json';
  static final _headers = {'Content-Type': 'application/json'};

  Future<Employee> createEmployee(Employee employee) async {
    try {
      String json = _toJson(employee);
      final response =
          await http.post(_serviceUrl, headers: _headers, body: json);
      var c = _fromJson(response.body);
      return c;
  
    } catch (e) {
      return null;
    }
  }

  getEmployees() async {
    try {
   
      var url = 'https://gyb-estimator.firebaseio.com/employees.json';
      var httpClient = new HttpClient();
      String result;
      var request = await httpClient.getUrl(Uri.parse(url));

      var response = await request.close();
      if (response.statusCode == HttpStatus.ok) {
        var json = await response.transform(utf8.decoder).join();
        Map<String, dynamic> decoded = jsonDecode(json);

        return decoded;
      }
    } catch (e) {
      return null;
    }
  }

  deleteEmployee(String key) async {
    try {
      final http.Response response = await http.delete(
          'https://gyb-estimator.firebaseio.com/employees/' + key + '.json');
      return response;
    } catch (e) {
      return null;
    }
  }

  Employee _fromJson(String json) {
    Map<String, dynamic> map = jsonDecode(json);
    var employee = new Employee();
    employee.name = map['name'];
    employee.firstname = map['firstname'];
    employee.mensualCost = map['mensualCost'];
    employee.mensualIncome = map['mensualIconme'];
    employee.contractType = map['contractType'];
    return employee;
  }

  String _toJson(Employee employee) {
    var mapData = new Map();
    mapData["name"] = employee.name;
    mapData["firstname"] = employee.firstname;
    mapData["mensualCost"] = employee.mensualCost;
    mapData["mensualIncome"] = employee.mensualIncome;
    mapData["contractType"] = employee.contractType;
    String json = jsonEncode(mapData);
    return json;
  }
}
